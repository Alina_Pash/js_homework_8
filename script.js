/*
Теоретичні питання

1. Опишіть своїми словами що таке Document Object Model (DOM)

   Document Object Model - це об'єктра браузерна модель, іншими словами - це наше оточення на веб сторінці.
   Він представляє собою об'єктну модель документа, яка усі елементи на сторінці бачить, як об'єкти. Завдяки цьому,
   ми можемо взаємодіяти з елементами на сторінці та впливати на їх поведінку.

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

   І innerHTML, і innerText є властивостями елементів HTML. Ми можемо змінити вміст елемента HTML за допомогою цих
   властивостей.
   Ми можемо призначити рядок HTML властивості innerHTML, яка відображається як звичайний HTML.
   Властивість innerText інтерфейсу HTMLElement представляє відтворений текстовий вміст вузла та його нащадків.  Він
   наближає текст, який користувач отримав би, якби виділив вміст елемента курсором, а потім скопіював його в буфер
   обміну. Як установник, це замінить дочірніх елементів елемента заданим значенням, перетворюючи будь-які розриви
   рядків на <br> елементи.
   Встановлення значення innerHTML видаляє всіх нащадків елемента та замінює їх вузлами, створеними шляхом аналізу HTML,
   указаного в рядку htmlString. Властивість innerHTML можна використовувати для перевірки поточного джерела HTML
   сторінки, включаючи будь-які зміни, внесені з моменту початкового завантаження сторінки.

3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

   - getElementById - cтворює посилання на елемент за його ідентифікатором id;
   - getElementByClassName - повертає об’єкт, подібний до масиву, усіх дочірніх елементів, які мають усі задані назви
класів;
   - getElementByTagName - метод getElementsByTagNameінтерфейсу Documentповертає HTMLCollectionелемент із заданим
   іменем тегу;
   - querySelector - id class tag повертає перший (Element) документа, який відповідає вказаному селектору або групі
селекторів;
   - querySelectorAll - повертає статичний (не динамічний), що містить усі знайдені елементи документа, які
   відповідають зазначеному селектору.querySelectorAll().
    Найбільш популярнішим методом є querySelectorAll. Він вважається найуніверсальнішим.
 */

// 1. Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const listParagraf = document.querySelectorAll('p')
listParagraf.forEach(item => item.style.backgraundColor = "#ff0000");

// 2. Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль.
// Знайти дочірні коди, якщо вони є, і вивести в консоль назви та тип нод.
const secondParagraph = document.getElementByID(elementId: "optionsList");
console.log(optList);

const optListParent = optList.parentElement;
console.log(optListParent);

if (optList.hasChildNodes()) {
    const optListChildren = optList.childNodes;
    optListChildren.forEach(item. => {
        console.log(item.nodeName, item.nodeType);
    })
};

// 3. Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
const testParagraph = document.querySelector('#testParagraph');
testParagraph.innerHTML = '<p>This is a paragraph</p>';

// 4. Отримати елементи вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти
// новий клас nav-item.
const headerElements = document.querySelectorAll(".main-header");
for (const headerElement of headerElements) {
    const headerElementChildren = headerElement.children;
    for (const elem of headerElementChildren) {
        console.log(elem);
        elem.classList.add("nav-item");
    }
};

// 5. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const sectionTitleList = document.querySelectorAll('.section-title');
sectionTitleList.forEach(
    function (el) {
        el.classList.remove('section-title');
    }
    );